#include"Customer.h"
#include<map>
#include <vector>
#define SIGN_AND_BUY 1
#define UPDATE 2
#define MAX_PAY 3
#define EXIT 4

#define RETURN_TO_MENU 0
#define MAX_ITEMS 10

#define BACK_TO_MENU 3
#define REMOVE_ITEMS 2
#define ADD_ITEMS 1

#define ARR_SIZE 10

/*functions declaration*/
int printMenuAndGetChoice();
void printMaxPayer(map<string, Customer> abcCustomers);
void manageSignAndBuy(map<string, Customer>& abcCustomers, Item* itemList);
bool checkIfCustomerExist(map<string, Customer> abcCustomers, string nameCust);
void addItem(map<string, Customer>& abcCustomers, Item* itemList, string custName);
bool checkIfItemExist(Customer customer, Item item, string custName);
void manageUpdate(map<string, Customer>& abcCustomers, Item* itemList);
void removeItem(map<string, Customer>& abcCustomers, Item* itemList, string custName);


int main()
{
	/*variables defintion*/
	int choice = 0;

	/*new map*/
	map<string, Customer> abcCustomers;

	/*the items in the store*/
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };


	do
	{
		/*the user is choosing what to do*/
		choice = printMenuAndGetChoice();

		switch (choice)
		{
			case SIGN_AND_BUY:
				manageSignAndBuy(abcCustomers, itemList); /*new customer*/
				break;
			case UPDATE:
				manageUpdate(abcCustomers, itemList); /*existing customer*/
				break;
			case MAX_PAY:
				printMaxPayer(abcCustomers); /*the customer who payed the most*/
				break;
			default:
				cout << "Bye, see you latter (:" << endl;
				break;
		}

		cout << endl;
		cout << endl;

	} while (choice != EXIT);

	
	system("pause");
	return 0;
}

/*The function prints the option and gets the customer choice*/
int printMenuAndGetChoice()
{
	/*variables definition*/
	int choice = 0;

	/*printing the options*/
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1. to sign as customer and buy items" << endl;
	cout << "2. to uptade existing customer's items" << endl;
	cout << "3. to print the customer who pays the most" << endl;
	cout << "4. to exit" << endl;

	cin >> choice;

	/*checking if the input is valid*/
	while (choice != SIGN_AND_BUY && choice != UPDATE && choice != MAX_PAY && choice != EXIT)
	{
		/*invalid input*/
		cout << "Invalid choice! Try again: " << endl;
		cin >> choice;
	}

	return choice;
}

/*The function prints who payed the most*/
void printMaxPayer(map<string, Customer> abcCustomers)
{
	/*checking if there is at least one customer*/
	if (abcCustomers.size() > 0)
	{
		/*variables definition*/
		map<string, Customer>::iterator it = abcCustomers.begin();
		Customer maxCust = it->second;
		double maxSum = maxCust.totalSum();
		string maxName = it->first;

		++it; /*we get the fisrt data already*/

		/*running over all the customers*/
		for (it; it != abcCustomers.end(); ++it)
		{
			if (it->second.totalSum() > maxSum) /*checking if someone payed more than the current max*/
			{
				/*changing the values according to the new max customer*/
				maxName = it->first;
				maxCust = it->second;
				maxSum = maxCust.totalSum();
			}
		}

		/*printing the results*/
		cout << "The max payer is: " << maxName << " and he or she paid: " << maxSum;
	}

}

/*The function manages the sign and buy option in the menu*/
void manageSignAndBuy(map<string, Customer>& abcCustomers, Item* itemList)
{
	/*variables definition*/
	bool check = false;
	string custName = "";
	int choice = 0;

	/*getting the customer name*/
	cout << "Please enter your name: ";
	cin >> custName;

	/*checking if the customer exist*/
	check = checkIfCustomerExist(abcCustomers, custName);

	if (check) /*the customer exist he or she doesn't need to sign again*/
	{
		cout << "The customer is already exist!" << endl;
	}
	else
	{
		/*adding the new customer to the map*/
		abcCustomers.insert(pair<string, Customer>(custName, Customer(custName)));
		/*going to add items if the user want to*/
		addItem(abcCustomers, itemList, custName);
	}
}


/*The function adds items to the customer's set*/
void addItem(map<string, Customer>& abcCustomers, Item* itemList, string custName)
{
	/*variables definiton*/
	int choice = 0;

	do
	{
		cout << "The items you can buy are: (0 to exit)" << endl;

		/*printing all the items*/
		for (int i = 0; i < ARR_SIZE; i++)
		{
			cout << i + 1 << ". " << itemList[i].getName() << "    price:" << itemList[i].getPrice() << endl;
		}

		cout << "What item would you like to buy? Input: ";
		cin >> choice;

		/*checking if the input is valid*/
		while (choice < RETURN_TO_MENU || choice > MAX_ITEMS)
		{
			cout << "Invalid choice! Try again: " << endl;
			cin >> choice;
		}

		if (choice != RETURN_TO_MENU)
		{
			/*checking if the item is not in the set*/
			if (abcCustomers.find(custName)->second.getItems().count(itemList[choice - 1]) == 0)
			{
				abcCustomers.find(custName)->second.addItem(itemList[choice - 1]); /*adding the item to the set*/
			}
			else
			{
				/*new item*/
				Item t = itemList[choice - 1];
				t.setCount(abcCustomers.find(custName)->second.getItems().find(itemList[choice - 1])->getCount() + 1); /*setting the count to the new value*/
				abcCustomers.find(custName)->second.removeItem(itemList[choice - 1]); /*removing the old item*/
				abcCustomers.find(custName)->second.addItem(t); /*adding the new item*/
			}
		}

		cout << endl;
		cout << endl;

	} while (choice != RETURN_TO_MENU);

}

/*The function checks if the customer is in the map*/
bool checkIfCustomerExist(map<string, Customer> abcCustomers, string nameCust)
{
	/*variables definition*/
	map<string, Customer>::iterator it = abcCustomers.begin();

	/*running over all the customers*/
	for (it = abcCustomers.begin() ; it != abcCustomers.end(); ++it)
	{
		if (!(it->first.compare(nameCust))) /*checking if the customer exist*/
		{
			return true; /*the customer found*/
		}
	}
	return false;
}

/*The function manages the update option in the menu*/
void manageUpdate(map<string, Customer>& abcCustomers, Item* itemList)
{
	/*variables definition*/
	bool check = false;
	string custName = "";
	int choice = 0;

	/*getting customer's name*/
	cout << "Please enter your name: ";
	cin >> custName;

	/*checking if the customer exist*/
	check = checkIfCustomerExist(abcCustomers, custName);

	if (!check) /*checking if the customer is not exist*/
	{
		cout << "The customer is not exist!" << endl;
	}
	else
	{
		cout << endl;

		/*printing the menu*/
		cout << "1. Add items" << endl;
		cout << "2. Remove items" << endl;
		cout << "3. Back to menu" << endl;

		/*getting the user's choice*/
		cout << "What would you like to do? Input: ";
		cin >> choice;

		/*checking if the input is invalid*/
		while (choice < ADD_ITEMS || choice > BACK_TO_MENU)
		{
			/*invlid input*/
			cout << "Invalid choice! Try again: " << endl;
			cin >> choice;
		}

		switch (choice)
		{
			case ADD_ITEMS:
				addItem(abcCustomers, itemList, custName); /*the customer chose to add items*/
				break;
			case REMOVE_ITEMS:
				removeItem(abcCustomers, itemList, custName); /*the customer chose to remove items*/
				break;
			default:
				break;
		}

	}
}

/*The function removes items from the customer's set*/
void removeItem(map<string, Customer>& abcCustomers, Item* itemList, string custName)
{
	/*variables definition*/
	int choice = 0;
	int numPrint = 0;
	vector<Item> itemsCanBeRemoved;

	do
	{
		cout << "The items you can remove are: (0 to exit)" << endl;

		/*running over all the items*/
		for (int i = 0; i < ARR_SIZE; i++)
		{
			if (abcCustomers.find(custName)->second.getItems().count(itemList[i]) > 0) /*checking if the item in the set*/
			{
				numPrint++;
				itemsCanBeRemoved.push_back(itemList[i]); /*adding the item to the vector that contains only the item the customer can remove*/
				cout << numPrint << ". " << itemList[i].getName() << "    price:" << itemList[i].getPrice() << endl; /*printing the items*/
			}
		}	

		if (numPrint == 0) /*checking if the user didn't add anything*/
		{
			cout << "You have no items!" << endl;
		}
		

		cout << "What item would you like to rmove? Input: ";
		cin >> choice;

		/*checking if the input is valid*/
		while (choice < RETURN_TO_MENU || choice > abcCustomers.find(custName)->second.getItems().size())
		{
			/*invalid input*/
			cout << "Invalid choice! Try again: " << endl;
			cin >> choice;
		}

		if (choice != RETURN_TO_MENU)
		{
			/*checking if only one of this item left*/
			if (abcCustomers.find(custName)->second.getItems().find(itemsCanBeRemoved[choice - 1])->getCount() == 1)
			{
				abcCustomers.find(custName)->second.removeItem(itemsCanBeRemoved[choice - 1]); /*removing the item from the set*/
			}
			else
			{
				/*new item*/
				Item t = itemsCanBeRemoved[choice - 1];
				t.setCount(abcCustomers.find(custName)->second.getItems().find(itemsCanBeRemoved[choice - 1])->getCount() - 1); /*changing the item's count to the new value*/
				abcCustomers.find(custName)->second.removeItem(itemsCanBeRemoved[choice - 1]); /*removing the old item*/
				abcCustomers.find(custName)->second.addItem(t); /*adding the new item*/
			}
		}

		cout << endl;
		cout << endl;

		numPrint = 0;

		/*cleaning the vector*/
		while (!itemsCanBeRemoved.empty())
		{
			itemsCanBeRemoved.pop_back();
		}

	} while (choice != RETURN_TO_MENU);
}