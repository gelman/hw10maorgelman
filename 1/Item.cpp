#include "Item.h"

/*ctor*/
Item::Item(string name, string serialNumber , double unitPrice)
{
	/*putting values in all the members*/
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}

/*dtor*/
Item::~Item()
{

}

/*setters*/
void Item::setCount(int n)
{
	this->_count = n;
}

/*getters*/
int Item::getCount() const
{
	return this->_count;
}
string Item::getName() const
{
	return this->_name;
}
double Item::getPrice() const
{
	return this->_unitPrice;
}

/*getting the total price of the item*/
double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

//compares the _serialNumber of those items.
bool Item::operator <(const Item& other) const
{
	return (this->_serialNumber.compare(other._serialNumber) < 0);
}

//compares the _serialNumber of those items.
bool Item::operator >(const Item& other) const
{
	return (this->_serialNumber.compare(other._serialNumber) > 0);
}

//compares the _serialNumber of those items.
bool Item::operator ==(const Item& other) const
{
	return (this->_serialNumber.compare(other._serialNumber) == 0);
}
