#include "Customer.h"

/*ctor*/
Customer::Customer(string name)
{
	this->_name = name;
}

/*dtor*/
Customer::~Customer()
{

}

//returns the total sum for payment
double Customer::totalSum() const
{
	/*variables definition*/
	set<Item>::iterator it;
	double sum = 0;

	/*running over all the items*/
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		sum += it->totalPrice(); /*adding to the sum*/
	}

	return sum;
}

//add item to the set
void Customer::addItem(Item item)
{
	this->_items.insert(item);
}

//remove item from the set
void Customer::removeItem(Item item)
{
	/*variables definition*/
	set<Item>::iterator it;

	/*running over all the items*/
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		if (!(it->getName().compare(item.getName()))) /*checking if this is the item we looking for*/
		{
			this->_items.erase(it); /*erasing the item*/
			break;
		}
	}

}

/*getters*/
set<Item> Customer::getItems() const
{
	return this->_items;
}
